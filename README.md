# IPynb Git Drop Output

Forked from
<https://github.com/denised/LandCover/blob/master/.ipynb_drop_output.py>

This script lets you keep IPython notebooks in git repositories. It tells git
to ignore prompt numbers, ids, execution numbers, and program outputs when
checking that a file has changed. 

Usage given in the script's docstring.
